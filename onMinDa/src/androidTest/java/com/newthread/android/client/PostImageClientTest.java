package com.newthread.android.client;

import android.os.Environment;
import android.os.SystemClock;
import android.test.ApplicationTestCase;
import android.test.suitebuilder.annotation.LargeTest;
import cn.bmob.v3.listener.FindListener;
import cn.bmob.v3.listener.SaveListener;
import com.bmob.BTPFileResponse;
import com.bmob.BmobProFile;
import com.bmob.btp.callback.UploadListener;
import com.newthread.android.activity.main.MyApplication;
import com.newthread.android.bean.remoteBean.bbs.Post;
import com.newthread.android.bean.remoteBean.bbs.PostImage_1;
import com.newthread.android.manager.BmobRemoteDateManger;
import com.newthread.android.util.Loger;

import java.io.File;
import java.util.List;

/**
 * Created by jindongping on 15/6/1.
 */
public class PostImageClientTest extends ApplicationTestCase<MyApplication> {


    public PostImageClientTest(Class<MyApplication> applicationClass) {
        super(MyApplication.class);
    }

    public PostImageClientTest() {
        super(MyApplication.class);
    }


    @Override
    public void setUp() throws Exception {
        super.setUp();
        createApplication();
    }

    @LargeTest
    public void testAdd50() throws Exception {
        for (int i = 0; i <13; i++) {
            testAdd();
        }
        SystemClock.sleep(10000);
    }


    @LargeTest
    public void testAdd() throws Exception {
        Post post = new Post();
        post.setObjectId("cfn6FHHX");
        PostImage_1 image = new PostImage_1();
        image.setPost(post);
        File file = new File(Environment.getExternalStorageDirectory() + "/123.png");
        if (!file.exists()) {
            Loger.V("文件不存在");
            return;
        }
        image.setImageSize(file.length()+"");
        upLoadHeadPhoto(file.getAbsolutePath(), image);
    }


    @LargeTest
    public void testUpdate() throws Exception {


        SystemClock.sleep(4000);
    }

    @LargeTest
    public void testQuerry() throws Exception {

        PostImage_1 image = new PostImage_1();
        Post post = new Post();
        post.setObjectId("fa4943ed3b");
        image.setPost(post);
        BmobRemoteDateManger.getInstance(getContext()).querry(image, new FindListener<PostImage_1>() {
            @Override
            public void onSuccess(List<PostImage_1> list) {
                Loger.V(list.size());
            }

            @Override
            public void onError(int i, String s) {

            }
        });

        SystemClock.sleep(4000);
    }

    private void upLoadHeadPhoto(String photoPath, final PostImage_1 image) {
        BTPFileResponse response = BmobProFile.getInstance(MyApplication.getInstance()).upload(photoPath, new UploadListener() {

            @Override
            public void onSuccess(String fileName, String url) {
                image.setImageUrl(url);
                image.setImageName(fileName);
                image.save(getContext(), new SaveListener() {
                    @Override
                    public void onSuccess() {
                        Loger.V("保存image成功");
                    }

                    @Override
                    public void onFailure(int i, String s) {

                    }
                });
            }

            @Override
            public void onProgress(int ratio) {
                Loger.V(ratio);
            }

            @Override
            public void onError(int statuscode, String errormsg) {

            }

        });
//        SystemClock.sleep(40000);
    }
}