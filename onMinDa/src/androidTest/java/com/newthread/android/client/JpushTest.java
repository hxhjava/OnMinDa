package com.newthread.android.client;

import android.os.SystemClock;
import android.test.ApplicationTestCase;
import android.test.suitebuilder.annotation.LargeTest;
import cn.jpush.im.android.api.JMessageClient;
import cn.jpush.im.android.api.model.Conversation;
import cn.jpush.im.android.api.model.Message;
import cn.jpush.im.api.BasicCallback;
import com.newthread.android.activity.main.MyApplication;
import com.newthread.android.util.Loger;

import java.util.List;

/**
 * Created by jindongping on 15/6/1.
 */
public class JpushTest extends ApplicationTestCase<MyApplication> {


    public JpushTest(Class<MyApplication> applicationClass) {
        super(MyApplication.class);
    }

    public JpushTest() {
        super(MyApplication.class);
    }


    @Override
    public void setUp() throws Exception {
        super.setUp();
        createApplication();
    }

    @LargeTest
    public void testLogin() throws Exception {
        JMessageClient.login("2012213738", "233233", new BasicCallback() {
            @Override
            public void gotResult(int i, String s) {
                Loger.V(i + " " + s);
            }
        });
        SystemClock.sleep(4000);
    }

    @LargeTest
    public void testLoginOut() throws Exception {
        JMessageClient.logout();
        SystemClock.sleep(4000);
    }

    @LargeTest
    public void testPushMessage() throws Exception {
        Message message = JMessageClient.createSingleTextMessage("2012213738", "哈哈");
        JMessageClient.sendMessage(message);
        SystemClock.sleep(4000);
    }

    @LargeTest
    public void testGetMessage() throws Exception {
        testPushMessage();
        List<Conversation> list = JMessageClient.getConversationList();
        for (Conversation c : list) {
            Loger.V(c);
        }

        SystemClock.sleep(4000);
    }

}