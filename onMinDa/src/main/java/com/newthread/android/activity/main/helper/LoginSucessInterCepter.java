package com.newthread.android.activity.main.helper;

import android.content.Context;
import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.listener.FindListener;
import cn.bmob.v3.listener.SaveListener;
import cn.jpush.im.android.api.JMessageClient;
import cn.jpush.im.android.api.model.UserInfo;
import cn.jpush.im.api.BasicCallback;
import com.newthread.android.bean.remoteBean.StudentUser;
import com.newthread.android.util.Loger;

import java.util.List;

/**
 * Created by jindongping on 15/5/31.
 * interCepteLoginCouresChart 这个方法会先从Bmob云去查用户是否存在，如果存在极光肯定存在就直接登陆极光，如果不存在，就先将用户上传到极光，
 * 上传极光可能存在已上传（i==898001）或者未上传，然后去学校网站解析用户信息，上传到bmob云，然后登陆极光并更新用户名到极光，如果上传失败，
 * 也没关系，下次接着调用该方法就行。
 *
 * 有可能存在没有更新用户名到极光，不过这并没有关系，只要保存用户信息到bmob云成功就行，极光推送需要的知识学号，用户名只是方便开发者查找。
 */
public class LoginSucessInterCepter {

    /**
     * 1.未注册将用户信息详细信息保存到bmob云，用户学号和姓名以及密码保存到极光，并登录极光
     * 2.注册用户直接登陆极光
     *
     * @param context
     * @param account
     * @param passWord
     */
    public static void interCepteLoginCouresChart(final Context context, final String account, final String passWord) {
        //登陆成功后 上传账号密码到Bmob云
        final StudentUser studentUser = new StudentUser(account, passWord);
        BmobQuery<StudentUser> query = new BmobQuery<StudentUser>();
        query.addWhereEqualTo("username", studentUser.getUsername());
        query.findObjects(context, new FindListener<StudentUser>() {
            @Override
            public void onSuccess(List<StudentUser> list) {
                if (list.size() != 0) {
                    Loger.V("用户已存在,登陆极光");
                    StudentUser studentUser = list.get(0);
                    JMessageClient.login(account,passWord, new BasicCallback() {
                        @Override
                        public void gotResult(int i, String s) {
                            if (i == 0) {
                                Loger.V("登陆极光成功");
                            } else {
                                Loger.V("登陆极光失败" + i + " " + s);
                            }
                        }
                    });

                } else {
                    saveToJiGuangAndBmob(context, account, passWord);
                }
            }

            @Override
            public void onError(int i, String s) {
                Loger.V("查询账号失败:" + i + "  " + s);
            }
        });

    }

    private static void saveToJiGuangAndBmob(final Context context, final String account, final String passWord) {

        final StudentUser studentUser = new StudentUser(account, passWord);

        JMessageClient.register(account, passWord, new BasicCallback() {
            @Override
            public void gotResult(int i, String s) {
                if (i == 0) {
                    Loger.V("上传JiGuang服务器成功");
                    //用户不存在，从学校网站 解析用户，获取更多信息
                    afterRegistJIGuang(studentUser, context, account, passWord);
                } else {
                    Loger.V("上传极光服务器失败" + i + " " + s);
                    if (i == 898001)
                        afterRegistJIGuang(studentUser, context, account, passWord);
                }
            }
        });

    }

    //在极光注册后，执行此方法 更新极光，上传Bmob,登陆极光
    private static void afterRegistJIGuang(final StudentUser studentUser, final Context context, final String account, final String passWord) {
        studentUser.fitStudentInfoFromSchool(new StudentUser.OnSetInfoListner() {
            @Override
            public void callBack(String result) {
                //如果获取用户信息没有失败  上传到Bmob云 和极光
                if (!result.contains("fail")) {
                    studentUser.signUp(context, new SaveListener() {
                        @Override
                        public void onSuccess() {
                            Loger.V("上传账号到Bmob服务器成功");
                            //先登陆极光 然后更新姓名
                            JMessageClient.login(account, passWord, new BasicCallback() {
                                @Override
                                public void gotResult(int i, String s) {
                                    if (i == 0) {
                                        UserInfo userInfo = JMessageClient.getMyInfo();
                                        userInfo.setNickname(studentUser.getName());
                                        JMessageClient.updateMyInfo(UserInfo.Field.nickname, userInfo, new BasicCallback() {
                                            @Override
                                            public void gotResult(int i, String s) {
                                                if (i == 0) {
                                                    Loger.V("更新极光推送姓名成功");
                                                } else {
                                                    Loger.V("更新极光推送姓名失败" + i + " " + s);
                                                }
                                            }
                                        });
                                    } else {
                                        Loger.V("登陆极光推送失败" + i + " " + s);
                                    }
                                }
                            });
                        }

                        @Override
                        public void onFailure(int i, String s) {
                            Loger.V("上传账号到Bmob服务器失败" + i + " " + s);
                        }
                    });
                }

            }
        });
    }
}
