package com.newthread.android.activity.main;

import android.app.Application;
import cn.bmob.v3.Bmob;
import cn.jpush.android.api.JPushInterface;
import cn.jpush.im.android.api.JMessageClient;
import cn.jpush.im.api.BasicCallback;
import com.newthread.android.util.Loger;
import com.newthread.android.util.MyPreferenceManager;

import java.util.HashMap;
import java.util.Map;

public class MyApplication extends Application {
    private static MyApplication instance;
    private Map<String, Object> temps;
    private static final String BMOBAPPKEY = "d3019bd206904d926e0e98b76d9a11a0";

    public static MyApplication getInstance() {
        return instance;
    }

    /**
     * 用于临时存放东西的方法
     *
     * @param name
     * @param obj
     */
    public void putThing(String name, Object obj) {
        temps.put(name, obj);
    }

    /**
     * r取出临时存放的东西，会自动remove掉
     *
     * @param name
     */
    public <T> T getThing(String name) {
        return (T) temps.remove(name);
    }

    @Override

    public void onCreate() {
        super.onCreate();
        instance = this;
        temps = new HashMap<>();
        Bmob.initialize(this, BMOBAPPKEY);
        InitJpush();
    }

    //Jpsuh的key是在manifest里面的mate属性指定的
    private void InitJpush() {
        JMessageClient.init(getApplicationContext());
        JPushInterface.setDebugMode(false);
        loginJpush();
    }

    /**
     * 登陆Jpush 如果账号密码为空则跳过
     */
    private void loginJpush() {
        MyPreferenceManager.init(getApplicationContext());
        String userName = MyPreferenceManager.getString("admin_system_account", "").trim();
        String passWord = MyPreferenceManager.getString("admin_system_password", "").trim();
        if (passWord.equals("") || passWord.length() < 1)
            return;
        JMessageClient.login(userName, passWord, new BasicCallback() {
            @Override
            public void gotResult(int i, String s) {
                if (i == 0) {
                    Loger.V("登陆极光成功");
                } else {
                    Loger.V("登陆极光失败" + i + " " + s);
                }
            }
        });
    }
}
