package com.newthread.android.mail;

/**
 * Created by 翌日黄昏 on 13-11-21.
 */
public class MailMs {
    private MailAuthor mailAuthor;
    private MailSetting mailSetting;
    private MailContent mailContent;
    private String to;

    public MailMs() {
        this.mailAuthor = null;
        this.mailSetting = null;
        this.mailContent = null;
        this.to = null;
    }

    public MailMs(MailAuthor mailAuthor, MailContent mailContent, String to) {
        this.mailAuthor = mailAuthor;
        this.mailContent = mailContent;
        this.to = to;
    }

    public MailAuthor getMailAuthor() {
        return mailAuthor;
    }

    public void setMailAuthor(MailAuthor mailAuthor) {
        this.mailAuthor = mailAuthor;
    }

    public MailContent getMailContent() {
        return mailContent;
    }

    public void setMailContent(MailContent mailText) {
        this.mailContent = mailText;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }
}
