package com.newthread.android.receiver;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.newthread.android.camera.FangDaoService;
import com.newthread.android.clock.TimeRemindService;
import com.newthread.android.util.Loger;

public class BootBroadcast extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent mintent) {
        if (Intent.ACTION_BOOT_COMPLETED.equals(mintent.getAction())) {
            Loger.V("开机广播开启FangDaoService");
            Intent service_1 = new Intent(context, FangDaoService.class);
            context.startService(service_1);
            Loger.V("开机广播开启TimeRemindService");
            Intent service_2 = new Intent(context, TimeRemindService.class);
            context.startService(service_2);
        }
    }

}
