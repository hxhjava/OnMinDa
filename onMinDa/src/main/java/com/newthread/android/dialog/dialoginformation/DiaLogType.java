package com.newthread.android.dialog.dialoginformation;

public enum DiaLogType {
	
	   CONFIRM(RequestCode.REQUEST_CODE_DIALOG_CONFIRM,PalyMusicDiaLog.class),
	;
	private int requestCode;
	private Class<?> dialogClassType;

	private DiaLogType(int requestCode, Class<?> dialogClassType) {
		this.requestCode = requestCode;
		this.dialogClassType = dialogClassType;
	}

	public int getRequestCode() {
		return requestCode;
	}

	public void setRequestCode(int requestCode) {
		this.requestCode = requestCode;
	}

	public Class<?> getDialogClassType() {
		return dialogClassType;
	}

	public void setDialogClassType(Class<?> dialogClassType) {
		this.dialogClassType = dialogClassType;
	}
}
